rm Mails

echo "*************************************************"
echo "1. Getting mail template content from processor's server repo"
echo "*************************************************"

mkdir Mails
mkdir Mails/Charges
cd Mails/Charges
curl -o first.html -k 'https://gitlab.com/api/v4/projects/27825923/repository/files/mails%2Fonboard.txt/raw?ref=main'
curl -o second.html -k 'https://gitlab.com/api/v4/projects/27825923/repository/files/mails%2Fonboard2.txt/raw?ref=main'
curl -o third.html -k 'https://gitlab.com/api/v4/projects/27825923/repository/files/mails%2Fonboard3.txt/raw?ref=main'

echo "*************************************************"
echo "1. Getting mail template content from payout's server repo"
echo "*************************************************"

cd ..
mkdir Payouts
cd Payouts



echo "*************************************************"
echo "1. Getting mail template content from onboarding server repo"
echo "*************************************************"

cd ..
mkdir Onboarding
cd  Onboarding
